const apiEndpoint = 'https://memberarea-api.edatinghub.com';
const redirectEndpoint = 'https://portal.datebook.gold';
const productDomain = window.location.host.startsWith("localhost") || window.location.origin === "file://" ? "www.datebook.gold" : window.location.host;
const phonePrefix = "+"; // "+49"

(function() {
  delete localStorage.cities
  delete localStorage.ip
  
  delete localStorage.coords
  delete localStorage.coordsAddress

  delete localStorage.product
  delete localStorage.subscriptions

  fetch('https://www.cloudflare.com/cdn-cgi/trace').then(res => {
    res.text().then(data => {
      let ipRegex = /[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}/
      const cfip = data.match(ipRegex)[0]
      if (cfip && cfip.includes(".")) {
        localStorage.ip = cfip
      } else {
        $.getJSON('https://api.db-ip.com/v2/free/self', function(data) {
          const dbip = JSON.stringify(data, null, 2)?.ipAddress
          if (dbip) {
            localStorage.ip = dbip
          } else {
            $.getJSON('http://www.geoplugin.net/json.gp', function(data) {
              const geoip = JSON.stringify(data, null, 2)?.geoplugin_request
              if (geoip) {
                localStorage.ip = geoip
              }
            })
          }
        })
      }
    })
  })

  document.addEventListener('DOMContentLoaded', () => {
    axios.get(apiEndpoint + '/v1/cities').then((resp) => {
      const cityList = document.getElementById('city-list')
      localStorage.cities = JSON.stringify(resp.data.cities)
      
      for (var i = 0; i < resp.data.cities.length; ++i) {
        var option = document.createElement('option')
        option.setAttribute('data-value', resp.data.cities[i].name)
        option.value = resp.data.cities[i].displayName
        cityList.appendChild(option)
      }
    })
  
    axios.get(apiEndpoint + '/v1/zips').then((resp) => {
      const zipList = document.getElementById('zip-list')
      
      for (var i = 0; i < resp.data.zips.length; ++i) {
        var option = document.createElement('option')
        option.setAttribute('data-value', resp.data.zips[i])
        option.value = resp.data.zips[i]
        zipList.appendChild(option)
      }
    })
  
    const yearList = document.getElementById('year-list')
    for (let year = new Date().getFullYear(); year >= 1930; year--) {
      var option = document.createElement('option')
      option.setAttribute('data-value', year.toString())
      option.value = year.toString()
      yearList.appendChild(option)
    }

    axios.get(apiEndpoint + `/v1/products?domain=${productDomain}`).then(resp => {
      const products = resp?.data?.products ?? []
      const prd = products[0]
      if (prd) {
        localStorage.product = JSON.stringify(prd)
        axios.get(apiEndpoint + `/v1/subscriptions?productName=${prd.name}`).then(resp => {
          localStorage.subscriptions = JSON.stringify(resp?.data?.subscriptions ?? [])
          
          refreshInfo(subscriptionDetails[0].code)
        })
      }
    })
  
    $("#phone").change(() => {
      let str = $("#phone").val()
      if (str.startsWith(phonePrefix)) {
        str = str.substring(phonePrefix.length)
      }
      str = str.replace(/[^0-9]/g, '')
      $("#phone").val(str)
    })

    setInterval(() => {
      if (!$(`#subscription`).val())
        return
      const detail = subscriptionDetails[parseInt($(`#subscription`).val())]
      const subscriptionCode = detail?.code
      if (!$(`#${subscriptionCode}-subscription`).hasClass(subscriptionImageClassesActive[true])) {
        for (let sn of subscriptionDetails.map(e => e.code)) {
          const activeCssName = subscriptionImageClassesActive[sn === subscriptionCode]
          const inactiveCssName = subscriptionImageClassesActive[sn !== subscriptionCode]
          $(`#${sn}-subscription`).removeClass(inactiveCssName)
          $(`#${sn}-subscription`).addClass(activeCssName)
        }
  
        refreshInfo(subscriptionCode)
      }
    }, 1000)
  })
}())

const zipChanged = () => {
  if (!localStorage.coords) {
    navigator.geolocation.getCurrentPosition(
      position => {
        if (position?.coords) {
          localStorage.coords = JSON.stringify({ latitude: position?.coords?.latitude, longitude: position?.coords?.longitude })
          axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${position?.coords?.latitude}&lon=${position?.coords?.longitude}&accept-language=de-DE&format=geocodejson`).then((resp) => {
            localStorage.coordsAddress = JSON.stringify((resp?.data?.features?.filter(e => e.type === "Feature") ?? [])[0]?.properties.geocoding)
          })
        }
      }, 
      err => console.log(err)
    )
  }
}

const getDeviceType = () => {
  const ua = navigator.userAgent;
  if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
    return "tablet";
  }
  if (
    /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
      ua
    )
  ) {
    return "mobile";
  }
  return "desktop";
}

const complete = ({ fields }) => {
  const months = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]

  const product = JSON.parse(localStorage.product)
  const subscriptions = JSON.parse(localStorage.subscriptions)
  const cities = JSON.parse(localStorage.cities)
  const coords = localStorage.coords ? JSON.parse(localStorage.coords) : null
  const coordsAddress = localStorage.coordsAddress ? JSON.parse(localStorage.coordsAddress) : null

  const gender = fields.filter(f => f.name === "Ich bin:")[0]?.value 
  const looking = fields.filter(f => f.name === "Ich suche:")[0]?.value
  const subscr = fields.filter(f => f.name === "Laufzeit:")[0]?.value
  const subscrDetail = subscriptionDetails.filter(e => e.monthsValue === subscr)[0]
  const subscrObj = subscriptions.filter(s => s.displayName.toLowerCase() === subscrDetail?.code)[0]
  const month = fields.filter(f => f.name === "Monat:")[0]?.value
  const cityName = fields.filter(f => f.name === "Ort:")[0]?.value
  const city = cities.filter(e => e.displayName === cityName)[0]
  const user = {
    sex: gender === "Mann" ? "man" : gender === "Frau" ? "woman" : null,
    lookingFor: looking === "Mann" ? "man" : looking === "Frau" ? "woman" : null,
    firstName: fields.filter(f => f.name === "Vorname:")[0]?.value,
    lastName: fields.filter(f => f.name === "Nachname:")[0]?.value,
    email: fields.filter(f => f.name === "E-Mail adresse:")[0]?.value,
    mobile: phonePrefix + fields.filter(f => f.name === "Telefonnummer:")[0]?.value,
    productName: product?.name,
    subscriptionName: subscrObj?.name,
    cityName: city?.name,
    stateName: city?.stateName,
    countryName: city?.countryName,
    zip: fields.filter(f => f.name === "PLZ:")[0]?.value,
    street: fields.filter(f => f.name === "Deine Strasse")[0]?.value,
    buildingNumber: fields.filter(f => f.name === "Nummer:")[0]?.value,
    dateOfBirth: {
      year: fields.filter(f => f.name === "Jahr:")[0]?.value,
      month: months.indexOf(month) + 1,
      day: fields.filter(f => f.name === "Tag")[0]?.value
    },
    brower: `${window.navigator.appCodeName} ${window.navigator.appVersion}`,
    os: window.navigator.platform,
    ip: localStorage.ip,
    device: getDeviceType(),
    geoLatitude: coords?.latitude ? coords?.latitude.toString() : null,
    geoLongitude: coords?.longitude ? coords?.longitude.toString() : null,
    geoCountryCode: coordsAddress?.country_code,
    geoStateDisplayName: coordsAddress?.state,
    geoCityDisplayName: coordsAddress?.city,
    geoStreet: coordsAddress?.street,
    geoBuildingNumber: coordsAddress?.housenumber,
    geoZip: coordsAddress?.postcode
  }
  
  axios.post(apiEndpoint + `/v1/users:register`, { user }).then(resp => {
    const { data } = resp
    const { login, confirmationCode } = data
    if (login && confirmationCode) {
      window.location = `${redirectEndpoint}/redirect?login=${login}&code=${confirmationCode}`
    }
  })
}

const subscriptionImageClassesActive = {
  true: 'css32',
  false: 'css33'
}

const subscriptionCodes = ["gold", "silver", "bronze"]
const subscriptionDetails = [{
  optionValue: 0,
  code: "gold",
  monthsValue: "24 Monate",
  months: 24,
  invoiceInfo: "jeweils für 12 Monate (im Voraus)"
}, {
  optionValue: 1,
  code: "silver",
  months: 12,
  monthsValue: "12 Monate",
  invoiceInfo: "1 und 11 Monate"
}, {
  optionValue: 2,
  code: "bronze",
  monthsValue: "6 Monate",
  months: 6,
  invoiceInfo: "1 und 5 Monate"
}]

const selectSubscription = (subscriptionCode) => {
  for (const sn of subscriptionDetails.map(e => e.code)) {
    const activeCssName = subscriptionImageClassesActive[sn === subscriptionCode]
    const inactiveCssName = subscriptionImageClassesActive[sn !== subscriptionCode]
    $(`#${sn}-subscription`).removeClass(inactiveCssName)
    $(`#${sn}-subscription`).addClass(activeCssName)
  }
  $(`#subscription`).val(subscriptionDetails.filter(e => e.code === subscriptionCode)[0]?.optionValue?.toString()).change()
  refreshInfo(subscriptionCode)
}

const refreshInfo = (code) => {
  const detail = subscriptionDetails.filter(e => e.code === code)[0]
  $("#inv").val(detail.invoiceInfo)
  const subscriptions = JSON.parse(localStorage.subscriptions)
  const subscription = subscriptions.filter(s => s.displayName.toLowerCase() === code)[0]
  const tariffPrice = subscription?.tariff?.value / Math.pow(10, -subscription?.tariff?.exponent)
  $("#pricePerMonth").val(`Nur €${(tariffPrice / detail.months).toFixed(2)} je Monat`)
  $("#price").val(`€${tariffPrice.toFixed(2)} (Laufzeit)`)
}

const onlyNumberKey = (evt) => {
  let ASCIICode = (evt.which) ? evt.which : evt.keyCode
  if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
    return false;
  return true;
}
